from webapp import webApp
from urllib import parse
import string
import random
import shelve

FORM= """
<hr>
<form action='/' method='POST'>
    <div>
        <label>INTRODUCE UNA URL PARA ACORTAR</label>
        <input type="text" name='URL' required>
    </div>
    <div>
        <input type="submit" name='SUBMIT'>
    </div>
</form>
"""

INITIAL_PAGE= """
<!DOCTYPE html>
<html lang='en'>
<head> 
    <title>RandomShort.com</title>
</head>
<body>
    <div>
        {form}
    </div>
    <div>
        <p>Lista de URLs: [{lista_url}]</p>
    </div>
</body>
</html>
"""

METHOD_NOT_ALLOWED = """
<!DOCTYPE html>
<html lang="en">
<body>
    <p>Metodo no permitido: {method}</p>
</body>
</html>
"""

PAGE_NOT_FOUND="""
<!DOCTYPE html>
<html lang='en'>
<body>
    <div>
        <p>Recurso no disponible {resource}</p>
    </div>
    <div>
        {form}
    </div>
    <div>
        <p>Lista de URLs: [{lista_url}]</p>
    </div>
</body>
</html>
"""

PAGE_UNPROCESSABLE = """
<!DOCTYPE html>
<html lang="en">
<body>
    <p>Unprocesable POST: {body}.</p>
</body>
</html>
"""

content = shelve.open('urls_dic')

class RandomShort(webApp):
    def parse(self, request):
        data = {}
        request = request.decode('utf-8')
        bodystart = request.find('\r\n\r\n')
        if bodystart == -1:
            data['body'] = None
        else:
            data['body'] = request[bodystart+4:]
        method = request.split(' ',2)[0]
        resource = request.split(' ',2)[1]
        data['method']= method
        data['resource']= resource
        print(data)
        return (data)

    def process(self, data):
        if data['method'] == 'GET':
            code, page = self.do_GET(data['resource'])
        elif data['method'] =='POST':
            code, page = self.do_POST(data['resource'], data['body'])
        else:
            code = "405 Method Not Allowed"
            page = METHOD_NOT_ALLOWED.format(method=data['method'])
        return (str(code), page)

    def do_GET(self, resource):
        if resource == '/':
            code = "200 Ok"
            page = INITIAL_PAGE.format(form=FORM, lista_url=self.diccionario_url(content))
        elif resource in content:
            url = content[resource]
            code = "301 Moved Permanently"
            page = f"HTTP/1.1 301 Moved Permanently\r\nLocation: {url}\r\n\r\n"
        elif resource == '/favicon.ico':
            code = "204 No Content"
            page = ''
        else:
            code = "404 Not Found"
            page = PAGE_NOT_FOUND.format(form=FORM, resource=resource, lista_url=self.diccionario_url(content))
        return code, page

    def do_POST(self, resource, body):
        args = parse.parse_qs(body)
        print(f'query:', args)
        if resource == '/':
            if 'URL' in args:
                url = self.completar_url(args['URL'][0])
                if url in content.values():
                    shorten_url = list(content.keys())[list(content.values()).index(url)]
                    print(shorten_url)
                    lista = self.diccionario_url(content)
                    page = INITIAL_PAGE.format(form=FORM, lista_url=lista)
                    code = "200 OK"
                else:
                    random_key = ''.join(random.choices(string.ascii_lowercase + string.digits, k=8))
                    shorten_url = 'https://' + random_key
                    content[shorten_url] = url
                    print(shorten_url)
                    lista = self.diccionario_url(content)
                    page = INITIAL_PAGE.format(form=FORM, lista_url=lista)
                    code = "200 Ok"
            else:
                page = PAGE_UNPROCESSABLE.format(body=body)
                code = "422 Unprocessable Entity"
        else:
            page = PAGE_UNPROCESSABLE.format(body=body)
            code = "422 Unprocessable Entity"
        return code, page

    def diccionario_url(self, content):
        diccionario = ""
        for element in content:
            diccionario = diccionario + "Url original: " + content[element] + " -->" + " Url acortada: " + \
                         """<a href=""" + content[element] + ">" + element + "</a>" + "<br>"
        return diccionario

    def completar_url(self, url):
        if 'http://' in url or 'https://' in url:
            return url
        else:
            return 'https://' + url

if __name__ == "__main__":
    webApp = RandomShort('localhost', 1234)
