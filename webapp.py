import socket

class webApp:
    def parse(self, request):
        return None

    def process(self, parsed_request):
        return ("200 Ok", "<html><body><h1>webApp funciona</h1></body></html>")
    def __init__(self, localhost, port):
        servidor = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        servidor.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        servidor.bind((localhost, port))
        servidor.listen(5)
        print('Sirviendo en puerto', str(port))

        while True:
            print('esperando conexiones...')
            conexion, direccion_cliente = servidor.accept()
            print(f'El cliente con direccion {direccion_cliente} se ha conectado')
            request = conexion.recv(2048)
            parsed_request = self.parse(request)
            (code, msg) = self.process(parsed_request)
            response = 'HTTP/1.1 ' + code + '\r\n\r\n' + msg + '\r\n'
            conexion.send(response.encode('utf-8'))
            conexion.close()

if __name__ == "__main__":
    webApp('localhost', 1234)